
const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const roleRoutes = require('./routes/role-route')
const permissionRoutes = require('./routes/permission-route')
const userRoutes = require('./routes/user-route')
const mailRoutes = require('./routes/mail-route')
const eventRoutes = require('./routes/event-route')
const taskRoutes = require('./routes/task-route')
const applicationHeaders = require('./helpers/header-middleware')
const authJwtMiddleWare = require('./helpers/auth-token-middleware')
const errorHanlder = require('./helpers/error-handler-middleware')
const app = express()
require('dotenv/config')

app.use(bodyParser.json())

app.use(morgan('tiny'))
app.use(cors({ origin: { global: true } }));
app.use(authJwtMiddleWare())
app.use(applicationHeaders)
app.use(errorHanlder.applicationErrorHandler)

const environmentConfigs = process.env
//routes
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.taskControllerRoute}`, taskRoutes)
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.permissionContrllerRoute}`, permissionRoutes)
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.roleControllerRoute}`, roleRoutes)
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.userControllerRoute}`, userRoutes)
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.mailControllerRoute}`, mailRoutes)
app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.eventControllerRoute}`, eventRoutes)

module.exports = app