const eventRepository = require("../../repository/event/event-repository");

exports.getEventDetails = async () => {
    try {
        const eventDetails = await eventRepository.getEventDetails();
        return {
            validity: true,
            result: eventDetails,
        };
    } catch (error) {
        throw error;
    }
};

exports.saveEvent = async (event) => {
    try {
        const savedResult = await eventRepository.saveEvent(event)
        if (savedResult) {
            return {
                validity: true,
                result: savedResult
            }
        } else {
            return {
                validity: false,
                result: 'Failed to save event.'
            }
        }
    } catch (error) {
        throw error
    }
};

exports.updateEvent = async (event) => {
    try {
        const updatedEvent = await eventRepository.updateEvent(event);
        return {
            validity: true,
            result: updatedEvent,
        };
    } catch (error) {
        throw error;
    }
};