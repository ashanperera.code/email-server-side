const mailRepository = require("../../repository/mail/mail-repository");

exports.saveSendMail = async (mailDetails) => {
    try {
        const mailEntity = mailDetails;
        const savedSendMail = await mailRepository.saveSendMail({ ...mailEntity });
        return {
            validity: true,
            result: savedSendMail,
        };
    } catch (error) {
        throw error;
    }
};

exports.saveReplyMail = async (replyMailDetails) => {
    try {
        const replyMailEntity = replyMailDetails;
        const savedReplyMail = await mailRepository.saveReplyMail({ ...replyMailEntity });
        return {
            validity: true,
            result: savedReplyMail,
        };
    } catch (error) {
        throw error;
    }
};

exports.getMailDetails = async () => {
    try {
        const mailDetails = await mailRepository.getMailDetails();
        return {
            validity: true,
            result: mailDetails,
        };
    } catch (error) {
        throw error;
    }
};