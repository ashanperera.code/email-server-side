const permissionRepository = require('../../repository/permission/permission-repository')

exports.savePermission = async (permission) => {
    try {
        const isExistsPermission = await permissionRepository.isExistsPermission(permission)
        if (isExistsPermission) {
            return {
                validity: false,
                error: 'Permission already exists'
            }
        } else {
            const savedPermission = await permissionRepository.savePermission(permission)
            return {
                validity: true,
                result: savedPermission
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getPermissionDetails = async () => {
    try {
        const permissionDetails = await permissionRepository.getPermissionDetails()
        return {
            validity: true,
            result: permissionDetails
        }
    } catch (error) {
        throw error
    }
}

exports.filterPermission = async (filterParams) => {
    try {
        const filteredPermissions = await permissionRepository.filterPermission({ ...filterParams })
        return {
            validity: true,
            result: filteredPermissions
        }
    } catch (error) {
        throw error
    }
}

exports.getPermission = async (permissionId) => {
    try {
        const permission = await permissionRepository.getPermission(permissionId)
        return {
            validity: true,
            result: permission
        }
    } catch (error) {
        throw error
    }
}

exports.updatePermission = async (permission) => {
    const updatedPermission = await permissionRepository.updatePermission(permission)
    return {
        validity: true,
        result: updatedPermission
    }
}

exports.deletePermission = async (permissionId, isRemove = false) => {
    const deleted = await permissionRepository.deletePermission(permissionId, isRemove)
    return {
        validity: true,
        result: deleted
    }
}