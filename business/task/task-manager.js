const taskRepository = require('../../repository/task/task-repository');

exports.saveTask = async (task) => {
    try {
        const isTaskExists = await taskRepository.isExistsTask(task)
        if (isTaskExists) {
            return {
                validity: false,
                error: 'Task already exists'
            }
        } else {
            const savedTask = await taskRepository.saveTask(task)
            return {
                validity: true,
                result: savedTask
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getTaskDetails = async () => {
    try {
        const taskDetails = await taskRepository.getAllTasks()
        return {
            validity: true,
            result: taskDetails
        }
    } catch (error) {
        throw error
    }
}

exports.filterTask = async (filterParams) => {
    try {
        const filteredTask = await taskRepository.filterTask({ ...filterParams })
        return {
            validity: true,
            result: filteredTask
        }
    } catch (error) {
        throw error
    }
}

exports.getTask = async (taskId) => {
    try {
        const task = await taskRepository.getTask(taskId)
        return {
            validity: true,
            result: task
        }
    } catch (error) {
        throw error
    }
}

exports.updateTask = async (task) => {
    const updatedTask = await taskRepository.updateTask(task)
    return {
        validity: true,
        result: updatedTask
    }
}

exports.deleteTask = async (taskId, isRemove = false) => {
    const deleted = await taskRepository.deleteTask(taskId, isRemove)
    return {
        validity: true,
        result: deleted
    }
}