const { v4: uuidv4 } = require("uuid");
const { body } = require("express-validator");
const { validationResult } = require("express-validator");
const eventManager = require("../../business/event/event-manager");
require("dotenv/config");

exports.validate = (method) => {
    switch (method) {
        case "saveEvent": {
            return [
                body("eventName", "event name is required.").exists().notEmpty(),
                body("eventStart", "event start date time is required.").exists().notEmpty(),
            ];
        }
        case "updateEvent":
            return [
                body("eventName", "event name is required.").exists().notEmpty(),
                body("eventStart", "event start date time is required.").exists().notEmpty(),
            ];
    }
};

exports.getEventDetails = async (req, res) => {
    try {
        const eventDetails = await eventManager.getEventDetails()
        if (eventDetails.validity && eventDetails.result && eventDetails.result.length > 0) {
            res.status(200).json(eventDetails)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.saveEvent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
        } else {
            const payload = req.body;
            if (payload) {
                const event = {
                    eventCode: uuidv4(),
                    eventName: payload.eventName,
                    eventDescription: payload.eventDescription,
                    eventStart: payload.eventStart,
                    eventEnd: payload.eventEnd,
                    assignee: payload.assignee,
                    isActive: true,
                    createdBy: payload.createdBy,
                    createdDate: payload.createdDate,
                };
                const savedResult = await eventManager.saveEvent(event);
                if (savedResult && savedResult.validity) {
                    res.status(201).json(savedResult);
                } else {
                    res.status(403).json({ error: savedResult.message, success: false });
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false });
            }
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false });
    }
};

exports.updateEvent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
        } else {
            const payload = req.body;
            if (payload) {
                const event = {
                    eventCode: payload.eventCode,
                    eventName: payload.eventName,
                    eventDescription: payload.eventDescription,
                    eventStart: payload.eventStart,
                    eventEnd: payload.eventEnd,
                    assignee: payload.assignee,
                    isActive: payload.isActive,
                    createdBy: payload.createdBy,
                    createdDate: payload.createdDate,
                };

                const updatedResult = await eventManager.updateEvent(event);
                if (updatedResult && updatedResult.validity) {
                    res.status(201).json(updatedResult);
                } else {
                    res.status(403).json({ error: e, success: false });
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false });
            }
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false });
    }
};