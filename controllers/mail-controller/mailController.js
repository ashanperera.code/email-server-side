const { v4: uuidv4 } = require("uuid");
const { body } = require("express-validator");
const nodemailer = require("nodemailer");
require("dotenv/config");
const startup = require('../../startup');
const mailManager = require("../../business/mail/mail-manager");

//sending mail
exports.sendMails = async (req, res) => {
    const payload = req.body;
    res.status(201).json(payload);

    var mailOptions = {
        from: payload.from, 
        to: payload.to, 
        subject: payload.subject, 
        html: payload.body,
    };

    await startup.transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            const sendMailPayload = {
                mailId: uuidv4(),
                from: payload.from,
                to: payload.to,
                subject: payload.subject,
                body: payload.body,
                attachments: payload.attachments
            };
            const sendResult = mailManager.saveSendMail(
                sendMailPayload
            );
            if (sendResult) {
                res.status(200).json(sendResult);
            } else {
                res.status(500).json({ error: sendResult.message, success: false });
            }
        }
    })
};

//replying to mail
exports.replyMails = async (req, res) => {
    const payload = req.body;
    res.status(201).json(payload);

    // send mail with defined transport object
    var mailOptions = {
        from: payload.from, // sender address
        to: payload.to, // receiver address
        subject: payload.subject, // Subject line
        text: payload.body, // plain text body
    };

    await startup.transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: %s", info.messageId);
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

            //save to DB
            const replyMailPayload = {
                replymailId: uuidv4(),
                mails: payload.mails,
                from: payload.from,
                to: payload.to,
                subject: payload.subject,
                body: payload.body,
                attachments: payload.attachments
            };

            const replyResult = mailManager.saveReplyMail(
                replyMailPayload
            );
            if (replyResult) {
                res.status(200).json(replyResult);
            } else {
                res.status(500).json({ error: replyResult.message, success: false });
            }
        }
    })
};

//get all mails
exports.getMailDetails = async (req, res) => {
    try {
        const mailDetails = await mailManager.getMailDetails();
        if (mailDetails) {
            res.status(200).json(mailDetails);
        } else {
            res.status(204).json({ error: 'Empty Dataset', success: false });
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false });
    }
};