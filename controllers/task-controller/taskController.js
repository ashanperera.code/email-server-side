const taskManager = require('../../business/task/task-manager');
const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator')
const { validationResult } = require('express-validator');

exports.validate = (methodName) => {
    switch (methodName) {
        case 'saveTask': {
            return [
                body('taskCode', 'task code is required.').exists().notEmpty(),
                body('taskStartingDate', 'starting date is required.').exists().notEmpty(),
            ]
        }
        case 'updateTask': {
            return [
                body('taskCode', 'task code is required.').exists().notEmpty(),
                body('taskStartingDate', 'starting date is required.').exists().notEmpty(),
            ]
        }
    }
}

exports.saveTask = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            const payload = req.body
            if (payload) {
                const task = {
                    taskId: uuidv4(),
                    taskCode: payload.taskCode,
                    taskDescription: payload.taskDescription,
                    taskDetails: payload.taskDetails,
                    taskStartingDate: payload.taskStartingDate,
                    taskDueDate:payload.taskDueDate,
                    assigneeName:payload.assigneeName,
                    isActive: true
                }
                const savedResult = await taskManager.saveTask(task)
                if (savedResult.validity) {
                    res.status(201).json(savedResult)
                } else {
                    res.status(500).json({ error: savedResult.error, success: false })
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.updateTask = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            const payload = req.body
            const task = {
                taskId: payload.taskId,
                taskCode: payload.taskCode,
                taskDescription: payload.taskDescription,
                taskDetails: payload.taskDetails,
                taskStartingDate: payload.taskStartingDate,
                taskDueDate:payload.taskDueDate,
                assigneeName:payload.assigneeName,
                isActive: true
            }
            const updatedResult = await taskManager.updateTask(task)
            if (updatedResult) {
                res.status(201).json(updatedResult)
            } else {
                res.status(500).json({ error: 'Failed to update.', success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getTask = async (req, res) => {
    try {
        const taskId = req.params.taskId
        if (taskId) {
            const task = await taskManager.getTask(taskId)
            if (task) {
                res.status(200).json(task)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(500).json({ error: "Task id is required.", success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getFilteredTasks = async (req, res) => {
    try {
        const filteredParams = req.body.filterParams
        if (filteredParams) {
            const taskDetails = await taskManager.filterTask(filteredParams)
            if (taskDetails) {
                res.status(200).json(taskDetails)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: "filteredParams are required.", success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}


exports.getAllTasks = async (req, res) => {
    try {
        const taskDetails = await taskManager.getTaskDetails()
        if (taskDetails) {
            res.status(200).json(taskDetails)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.deleteTask = async (req, res) => {
    try {
        const taskId = req.params.taskId
        const isRemove = req.params.isRemove
        if (taskId) {
            const deleted = await taskManager.deleteTask(taskId, isRemove)
            if (deleted) {
                res.status(200).json(deleted)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'Failed to delete.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}