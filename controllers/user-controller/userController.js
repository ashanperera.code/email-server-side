const { v4: uuidv4 } = require("uuid");
const { body } = require("express-validator");
const { validationResult } = require("express-validator");
const userManager = require("../../business/user/user-manager");
require("dotenv/config");

exports.validate = (method) => {
  switch (method) {
    case "saveUser": {
      return [
        body("userName", "user name is required.").exists().notEmpty(),
        body("userEmail", "user email is required.").exists().notEmpty(),
        body("password", "user password is required.").exists().notEmpty(),
        body("nic", "Nic is required").exists().notEmpty(),
      ];
    }
    case "updateUser":
      return [
        body("userName", "user name is required.").exists().notEmpty(),
        body("userEmail", "user email is required.").exists().notEmpty(),
        body("nic", "Nic is required").exists().notEmpty(),
      ];
    case "UserAuthentication":
      return [
        body("userName", "user name is required.").exists().notEmpty(),
        body("password", "user password is required.").exists().notEmpty(),
      ];
    case "changePassword":
      return [
        body("email", "email is required.").exists().notEmpty(),
        body("email", "invalid email.").exists().isEmail(),
        body("password", "password is required.").exists().notEmpty(),
      ];
  }
};

exports.changePassword = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
      return;
    } else {
      const payload = req.body;
      const changePasswordPayload = {
        email: payload.email,
        password: payload.password,
      };
      const userResult = await userManager.changeUserPassword(
        changePasswordPayload
      );
      if (userResult && userResult.validity) {
        res.status(200).json(userResult);
      } else {
        res.status(500).json({ error: userResult.message, success: false });
      }
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.UserAuthentication = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
      return;
    } else {
      const payload = req.body;
      if (payload) {
        const userPaylod = {
          userName: payload.userName,
          userEmail: payload.userEmail,
          password: payload.password,
        };
        const UserAuthentication = await userManager.authenticateUser(
          userPaylod
        );
        if (UserAuthentication && UserAuthentication.validity) {
          res.status(200).json(UserAuthentication);
        } else {
          res.status(401).json(UserAuthentication);
        }
      } else {
        res.status(400).json({ error: "Invalid model", success: false });
      }
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.saveUser = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
      return;
    } else {
      const payload = req.body;
      if (payload) {
        const user = {
          userId: uuidv4(),
          userName: payload.userName,
          firstName: payload.firstName,
          lastName: payload.lastName,
          middleName: payload.middleName,
          userEmail: payload.userEmail,
          password: payload.password,
          contact: payload.contact,
          userAddress: payload.userAddress,
          nic: payload.nic,
          passportId: payload.passportId,
          roles: payload.roles,
          permission: payload.permission,
          isActive: true,
        };
        const savedResult = await userManager.saveUser(user);
        if (savedResult && savedResult.validity) {
          res.status(201).json(savedResult);
        } else {
          res.status(500).json({ error: savedResult.message, success: false });
        }
      } else {
        res.status(400).json({ error: "Invalid model", success: false });
      }
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.getUserDetails = async (req, res) => {
  try {
    const userDetails = await userManager.getUserDetails();
    if (userDetails.validity) {
      res.status(200).json(userDetails);
    } else {
      res.status(204).json();
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.getUserDetail = async (req, res) => {
  try {
    const userId = req.params.userId;
    if (userId) {
      const user = await userManager.getUser(userId);
      if (user.validity) {
        res.status(200).json(user);
      } else {
        res.status(204).json();
      }
    } else {
      res.status(400).json({ error: "user id required.", success: false });
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.getFilteredUser = async (req, res) => {
  try {
    const filteredParams = req.body.filterParams;
    if (filteredParams) {
      const userDetails = await userManager.filterUser(filteredParams);
      if (userDetails && userDetails.validity) {
        res.status(200).json(userDetails);
      } else {
        res.status(204).json();
      }
    } else {
      res
        .status(400)
        .json({ error: "Filter params are required.", success: false });
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.updateUser = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
      return;
    } else {
      const payload = req.body;
      if (payload) {
        const user = {
          userId: payload.userId,
          userName: payload.userName,
          firstName: payload.firstName,
          lastName: payload.lastName,
          middleName: payload.middleName,
          userEmail: payload.userEmail,
          password: payload.password,
          contact: payload.contact,
          userAddress: payload.userAddress,
          nic: payload.nic,
          passportId: payload.passportId,
          roles: payload.roles,
          permission: payload.permission,
          isActive: payload.isActive,
        };

        const updatedResult = await userManager.updateUser(user);
        if (updatedResult && updatedResult.validity) {
          res.status(201).json(updatedResult);
        } else {
          res.status(500).json({ error: e, success: false });
        }
      } else {
        res.status(400).json({ error: "Invalid model", success: false });
      }
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.getUserRolePermission = async (req, res) => {
  try {
    const userId = req.params.userId;
    if (userId) {
      const userRolePermission = await userManager.getUserRolePermission(
        userId
      );
      if (
        userRolePermission.validity &&
        userRolePermission.result &&
        userRolePermission.result.length > 0
      ) {
        res.status(201).json(userRolePermission);
      } else {
        res.status(204).json();
      }
    } else {
      res.status(400).json({ error: "user id is required.", success: false });
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const userId = req.params.userId;
    const isRemove = req.params.isRemove;
    if (userId) {
      const deleted = await userManager.deleteUser(userId, isRemove);
      if (deleted && deleted.validity) {
        res.status(200).json(deleted);
      } else {
        res.status(204).json();
      }
    } else {
      res.status(400).json({ error: "user id is required.", success: false });
    }
  } catch (error) {
    res.status(500).json({ error: error, success: false });
  }
};
