require("dotenv/config");
const expressJwt = require("express-jwt");

const authJwtMiddleWare = () => {
  const environmentConfigs = process.env;
  return expressJwt({
    secret: environmentConfigs.applicationSecret,
    algorithms: ["HS256"],
  }).unless({
    path: [
      `${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.userControllerRoute}${environmentConfigs.userSignInAction}`,
      `${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.userControllerRoute}${environmentConfigs.changePassword}`,
      `${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.userControllerRoute}${environmentConfigs.userSignUpAction}`,
    ],
  });
};

module.exports = authJwtMiddleWare;
