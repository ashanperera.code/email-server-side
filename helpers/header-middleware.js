const headReader = require('./header-reader')

module.exports = function applicationHeaders(req, res, next) {
    const user = req.header('x-user');
    if (user) {
        headReader.setHeaderDetails({ user: user })
    }
    next();
};

