const headerDetails = {
    user: String
}

const header = Object.create(headerDetails)

exports.setHeaderDetails = (details) => {
    header.user = details.user
}

exports.getHeaderDetails = () => { 
    return header
}