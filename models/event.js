const mongoose = require("mongoose");

const eventSchema = mongoose.Schema({
    eventCode: { type: String },
    eventName: { type: String },
    eventDescription: { type: String },
    eventStart: { type: String },
    eventEnd: { type: String },
    assignee: { type: String },
    isActive: { type: Boolean },
    createdBy: { type: String },
    createdDate: { type: String },
});

const Event = mongoose.model("Event", eventSchema);
module.exports = Event;
