const mongoose = require("mongoose");

const mailSchema = mongoose.Schema({
    mailId: { type: String },
    to: { type: String },
    from: { type: String },
    subject: { type: String },
    body: { type: String },
    attachments: { type: String },
    //createdBy
    //createdOn
    //isActive
    
});

const Mail = mongoose.model("Mail", mailSchema);
module.exports = Mail;
