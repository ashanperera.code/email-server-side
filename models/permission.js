const mongoose = require('mongoose')

const permissionSchema = mongoose.Schema({
    permissionId: { type: String },
    permissionCode: { type: String },
    permissionName: { type: String },
    permissionDescription: { type: String },
    isActive: { type: Boolean }
})

const Permission = mongoose.model('Permission', permissionSchema)
module.exports = Permission;