const mongoose = require('mongoose')

const replyMailSchema = mongoose.Schema({
    replymailId: { type: String },
    mails: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Mail",
        required: true
    },
    to: { type: String },
    from: { type: String },
    subject: { type: String },
    body: { type: String },
    attachments: { type: String },
})

const replyMail = mongoose.model('replyMail', replyMailSchema)
module.exports = replyMail