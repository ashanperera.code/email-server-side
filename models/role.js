const mongoose = require('mongoose')

const roleSchema = mongoose.Schema({
    roleId: { type: String },
    roleCode: { type: String },
    roleName: { type: String },
    roleDescription: { type: String },
    permissions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Permission', required: true }],
    isActive: { type: Boolean }
})

const Role = mongoose.model('Role', roleSchema)
module.exports = Role;