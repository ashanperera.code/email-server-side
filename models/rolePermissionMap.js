const mongoose = require('mongoose')

const rolePermissionMapSchema = mongoose.Schema({
    id: { type: String },
    roles: { type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true },
    permissions: { type: mongoose.Schema.Types.ObjectId, ref: 'Permission', required: true }
})

const RolePermission = mongoose.model('RolePermission', rolePermissionMapSchema)
module.exports = RolePermission