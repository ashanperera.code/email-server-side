const mongoose = require("mongoose");

const taskSchema = mongoose.Schema({
    taskId: { type: String },
    taskCode: { type: String },
    taskDescription: { type: String },
    taskDetails: { type: String },
    taskStartingDate: { type: Date },
    taskDueDate: { type: Date },
    assigneeName: { type: String },
    createdBy: { type: String },
    createdOn: { type: Date },
    modifiedBy: { type: String },
    modifiedOn: { type: Date },
    isActive: { type: Boolean }
});

const Task = mongoose.model("Task", taskSchema);
module.exports = Task;
