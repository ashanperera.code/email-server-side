const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  userId: { type: String },
  userName: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  middleName: { type: String },
  userEmail: { type: String },
  password: { type: String },
  passwordSalt: { type: String },
  contact: { type: String },
  userAddress: { type: String },
  nic: { type: String },
  passportId: { type: String },
  roles: [
    { type: mongoose.Schema.Types.ObjectId, ref: "Role", required: false },
  ],
  isActive: { type: Boolean },
});

const User = mongoose.model("User", userSchema);
module.exports = User;
