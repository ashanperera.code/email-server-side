const eventModel = require('../../models/event')

exports.getEventDetails = async () => {
    return await eventModel.find({ isActive: true })
}

exports.saveEvent = async (event) => {
    return await eventModel.create(event)
}

exports.updateEvent = async (event) => {
    const options = { upsert: true };
    const filter = { eventCode: event.eventCode };
    const updateDoc = {
        $set: { ...event },
    };
    return await eventModel.updateOne(filter, updateDoc, options)

}