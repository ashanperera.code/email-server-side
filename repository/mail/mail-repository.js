const mailModel = require("../../models/mail");
const replyMailModel = require("../../models/replyMail");
require("dotenv/config");

exports.saveSendMail = async (mailDetails) => {
    const createdMailResult = await mailModel.create(mailDetails);
    return createdMailResult;
};

exports.saveReplyMail = async (replyMailDetails) => {
    const createdReplyMailResult = await replyMailModel.create(replyMailDetails);
    return createdReplyMailResult;
};

exports.getMailDetails = async () => {
    return await mailModel.find();
};