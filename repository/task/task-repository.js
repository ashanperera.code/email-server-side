const taskModel = require('../../models/task');
const headerReader = require('../../helpers/header-reader');

exports.saveTask = async (task) => {
    const details = headerReader.getHeaderDetails();
    task.createdBy = details.user;
    task.createdOn = new Date();
    task.modifiedBy = "";
    task.modifiedOn = null;
    return await taskModel.create(task);
}

exports.getAllTasks = async () => {
    const tasks = await taskModel.find({ isActive: true }).sort({ createdOn: 'descending' })
    return tasks
}

exports.isExistsTask = async (task) => {
    const query = { $or: [{ taskId: task.taskId }, { taskCode: task.taskCode }] }
    return await taskModel.exists(query)
}

exports.filterTask = async (filterParams) => {
    return await taskModel.find({ ...filterParams })
}

exports.getTask = async (taskId) => {
    return await taskModel.findOne({ taskId: taskId });
}

exports.updateTask = async (task) => {
    const details = headerReader.getHeaderDetails();
    task.modifiedBy = details.user;
    task.modifiedOn = new Date();

    const options = { upsert: true }
    const filter = { taskId: task.taskId }
    const updateDoc = {
        $set: { ...task },
    };
    return await taskModel.updateOne(filter, updateDoc, options)
}

exports.deleteTask = async (taskId, isRemove = false) => {
    if (isRemove === 'true') {
        return await this.removeRecord(taskId)
    } else {
        const task = await this.getTask(taskId)
        if (task) {
            task.isActive = false
            const currentDoc = task._doc
            if (currentDoc) {
                const updatedTask = await this.updateTask(currentDoc)
                if (updatedTask) {
                    return true
                } else {
                    return false
                }
            }
        }
    }
}

exports.removeRecord = async (taskId) => {
    const filter = { taskId: taskId };
    const result = await taskModel.deleteOne(filter)
    if (result) {
        return true
    }
    return false
}