const userModel = require("../../models/user");
const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");
require("dotenv/config");

const environmentConfigs = process.env;

exports.saveUser = async (user) => {
  const createdResult = await userModel.create(user);
  const passwordInBinary = environmentConfigs.passwordSecret;
  createdResult[
    "password"
  ] = `${passwordInBinary}@!${uuidv4()}#%#${await bcrypt.genSalt(10)}`;
  createdResult["passwordSalt"] = `${passwordInBinary}###${await bcrypt.genSalt(
    10
  )}###${uuidv4()}`;
  return createdResult;
};

exports.getUserDetails = async () => {
  return await userModel
    .find({ isActive: true })
    .select("-password -passwordSalt");
};

exports.isExists = async (filterParams) => {
  const query = {
    $or: [
      { userName: filterParams.userName },
      { userEmail: filterParams.userEmail },
      { nic: filterParams.nic },
    ],
  };
  return await userModel.exists(query);
};

exports.filterUser = async (filterParams) => {
  return await userModel
    .find({ ...filterParams, isActive: true })
    .select("-password -passwordSalt");
};

exports.getUserDetail = async (userId) => {
  return await userModel
    .find({ userId: userId })
    .populate({
      path: "roles",
      model: "Role",
      select: "-_id -__v",
      match: { isActive: true },
      populate: {
        path: "permissions",
        match: { isActive: true },
        select: "-_id -__v -permissionId",
        model: "Permission",
      },
    })
    .select("-password -passwordSalt -__v");
};

exports.getUserRolePermissions = async (userId) => {
  const userRole = await userModel
    .find({ userId: userId })
    .populate({
      path: "roles",
      model: "Role",
      select: "-_id -__v",
      match: { isActive: true },
      populate: {
        path: "permissions",
        match: { isActive: true },
        select: "-_id -__v -permissionId",
        model: "Permission",
      },
    })
    .select("_id");
  return userRole;
};

exports.updateUser = async (user) => {
  const options = { upsert: true };
  const filter = { userId: user.userId };

  const currentUser = await this.getUserDetail(user.userId);
  if (currentUser) {
    currentUser.userEmail = user.userEmail;
    currentUser.contact = user.contact;
    currentUser.userAddress = user.userAddress;
    currentUser.nic = user.nic;
    currentUser.passportId = user.passportId;
    currentUser.roles = user.roles;

    const updateDoc = {
      $set: {
        ...currentUser,
      },
    };
    const updatedUser = await userModel.updateOne(filter, updateDoc, options);
    updatedUser["password"] = null;
    updatedUser["passwordSalt"] = null;
    return updatedUser;
  }
};

exports.deleteUser = async (userId, isRemove = false) => {
  if (isRemove === "true") {
    return await this.removeRecord(userId);
  } else {
    const user = await this.getUserDetail(userId);
    if (user) {
      user.isActive = false;
      const currentDoc = user._doc;
      if (currentDoc) {
        const updatedUser = await this.updateUser(currentDoc);
        if (updatedUser) {
          return true;
        } else {
          return false;
        }
      }
    }
  }
};

exports.findUserForAuthentication = async (user) => {
  const query = {
    $or: [{ userEmail: user.userEmail }, { userName: user.userName }],
  };
  const searchedUser = await userModel.findOne(query).populate({
    path: "roles",
    model: "Role",
    select: "-_id -__v",
    match: { isActive: true },
    populate: {
      path: "permissions",
      match: { isActive: true },
      select: "-_id -__v -permissionId",
      model: "Permission",
    },
  });
  return searchedUser;
};

exports.removeRecord = async (userId) => {
  const filter = { userId: userId };
  const result = await userModel.deleteOne(filter);
  if (result) {
    return true;
  }
  return false;
};

exports.changePassword = async (passwordChangeRequest) => {
  const user = (
    await this.filterUser({ userEmail: passwordChangeRequest.email })
  )[0];
  if (user) {
    const filter = { _id: user._id };
    user.password = passwordChangeRequest.password;
    user.passwordSalt = passwordChangeRequest.passwordSalt;
    const updateDoc = {
      $set: {
        ...user,
      },
    };
    await userModel.updateOne(filter, updateDoc);
    user.password = null;
    user.passwordSalt = null;
    return {
      message: "updated successfully.",
    };
  }
  return null;
};
