const express = require("express");
require("dotenv/config");
const router = express.Router();
const eventController = require("../controllers/event-controller/eventController");

const environmentConfigs = process.env;

//routes
router.get(`${environmentConfigs.eventDetailsAction}`, eventController.getEventDetails)
router.post(`${environmentConfigs.eventCreateAction}`, eventController.validate('saveEvent'), eventController.saveEvent)
router.put(`${environmentConfigs.eventUpdateAction}`, eventController.validate('updateEvent'), eventController.updateEvent)

module.exports = router;