const express = require("express");
require("dotenv/config");
const router = express.Router();
const mailController = require("../controllers/mail-controller/mailController");

const environmentConfigs = process.env;

//routes
router.get(`${environmentConfigs.getAllMail}`, mailController.getMailDetails)
router.post(`${environmentConfigs.sendMail}`, mailController.sendMails)
router.post(`${environmentConfigs.replyMail}`, mailController.replyMails)

module.exports = router;