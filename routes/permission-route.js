const express = require('express')
const router = express.Router()
require('dotenv/config')
const permissionController = require('../controllers/permission-controller/permissionController')

const environmentConfigs = process.env

router.get(`${environmentConfigs.permissionDetailsAction}`, permissionController.getPermissionDetails)
router.get(`${environmentConfigs.permissionDetailAction}`, permissionController.getPermissionDetail)
router.post(`${environmentConfigs.permissionCreateAction}`, permissionController.validate('savePermission'), permissionController.savePermission)
router.post(`${environmentConfigs.permissionFilterAction}`, permissionController.getFilteredPermission)
router.put(`${environmentConfigs.permissionUpdateAction}`, permissionController.validate('updatePermission'), permissionController.updatePermission)
router.delete(`${environmentConfigs.permissionDeleteAction}`, permissionController.deletePermission)


module.exports = router;