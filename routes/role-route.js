const express = require('express')
const router = express.Router()
require('dotenv/config')
const roleController = require('../controllers/role-controller/roleController')

const environmentConfigs = process.env

router.get(`${environmentConfigs.roleDetailsAction}`, roleController.geRoleDetails)
router.get(`${environmentConfigs.roleDetailAction}`, roleController.getRoleDetail)
router.post(`${environmentConfigs.roleCreateAction}`, roleController.validate('saveRole'), roleController.saveRole)
router.post(`${environmentConfigs.roleFilterationAction}`, roleController.getFilteredRole)
router.put(`${environmentConfigs.roleUpdateAction}`, roleController.validate('updateRole'), roleController.updateRole)
router.delete(`${environmentConfigs.roleDeleteAction}`, roleController.deleteRole)


module.exports = router;