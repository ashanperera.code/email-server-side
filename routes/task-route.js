const express = require('express')
const router = express.Router()
require('dotenv/config')
const taskController = require('../controllers/task-controller/taskController');

const environmentConfigs = process.env

router.get(`${environmentConfigs.getAllTasks}`, taskController.getAllTasks)
router.get(`${environmentConfigs.getTask}`, taskController.getTask)
router.post(`${environmentConfigs.createTask}`, taskController.validate('saveTask'), taskController.saveTask)
router.post(`${environmentConfigs.filterTask}`, taskController.getFilteredTasks)
router.put(`${environmentConfigs.updateTask}`, taskController.validate('updateTask'), taskController.updateTask)
router.delete(`${environmentConfigs.deleteTask}`, taskController.deleteTask)


module.exports = router;