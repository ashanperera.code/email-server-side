const express = require("express");
require("dotenv/config");
const router = express.Router();
const userController = require("../controllers/user-controller/userController");

const environmentConfigs = process.env;

router.put(`${environmentConfigs.changePassword}`, userController.validate('changePassword'), userController.changePassword)
router.get(`${environmentConfigs.userDetailsAction}`, userController.getUserDetails)
router.get(`${environmentConfigs.userRolePermissionAction}`, userController.getUserRolePermission)
router.get(`${environmentConfigs.userDetailAction}`, userController.getUserDetail)
router.post(`${environmentConfigs.userSignUpAction}`, userController.validate('saveUser'), userController.saveUser)
router.post(`${environmentConfigs.userSignInAction}`, userController.validate('UserAuthentication'), userController.UserAuthentication)
router.post(`${environmentConfigs.userFilterationAction}`, userController.getFilteredUser)
router.put(`${environmentConfigs.userUpdateAction}`, userController.validate('updateUser'), userController.updateUser)
router.delete(`${environmentConfigs.userDeleteAction}`, userController.deleteUser)

module.exports = router;
