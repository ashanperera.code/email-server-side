const app = require('./app')
const mongoose = require('mongoose')
require('dotenv/config')

//NodeMailer
const nodemailer = require("nodemailer");

const environmentVariables = process.env

if (environmentVariables) {
    //database connectivity
    const dbConnection = `mongodb+srv://${environmentVariables.databaseUser}:${environmentVariables.databasePassword}@cluster0.hchdi.mongodb.net/${environmentVariables.databaseName}?retryWrites=true&w=majority`;
    mongoose.connect(`${dbConnection}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: environmentVariables.databaseName
    }).then(() => {
        console.log(`Database is up and running............`);
    }).catch(e => {
        console.log(e);
    })

    app.listen(environmentVariables.hostListenerPort, () => {
        console.log(`${environmentVariables.applicationServerUpGreeting} ${environmentVariables.hostListenerPort}`);
    });

    exports.transporter = nodemailer.createTransport({
        host: environmentVariables.MailHost,
        port: environmentVariables.MailListenerPort,
        secure: false,
        auth: {
            user: environmentVariables.MailUser,
            pass: environmentVariables.MailPass,
        },
        tls: {
            rejectUnauthorized: false,
        },
    });

} else {
    console.log('Failed to load application.....');
}

